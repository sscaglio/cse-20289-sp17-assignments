#!/bin/sh

usage() {
    cat <<EOF
Usage: $(basename $0) [-n N -D]

    -n N	Returns N items (default is 5)
    -D		Rank in descending order.
EOF
    exit $1
}

read STDIN
N=5
D=0

while getopts "hDn:" OPTION
do
	case $OPTION in
		h)
			usage
			;;
		D)
			D=1
			;;
		n)
			N=$OPTARG
			;;
	esac
done

ranking() {
	if [ $D -eq 1 ]; then
		sort -n $STDIN | head -$N
	else
		sort -n $STDIN | head -$N
	fi
}

ranking
