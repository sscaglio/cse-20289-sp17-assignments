GRADING - Exam 01
=================

- Commands:         1.25
- Short Answers:
    - Files:        2.25
    - Processes:    2.75
    - Pipelines:    2.5
- Debugging:        3
- Code Evaluation:  2.5
- Filters:          2.75
- Scripting:        2.75
- Total:	    19.75
