Homework 02 - Grading
====================

**Score**: 13.75/15

Deductions
----------
extract.sh
-1 tests failed

fortune.sh
-.25 cowsay did not run because path was not specified

Comments
--------
instead of using web browser you can use curl or wget through the terminal. Otherwise your oracle journey was an inspiring account. Great job!
