Homework 02
===========
Activity 1
1. I tried to use if [ $# -eq 0 ] to check if the number of
	input arguments the script was passed was equal to
	zero but I continued to get the same error that the
	script does not accept empty arguments. I then
	tried if [ -z "${n}" ] to check if the expansion of
	${n} is a null string or not but again recieved the same
	error. Next, I tried to move the first attempt ( if
	[ $# -eq 0 ] outside of the loop to see if it made a
	difference. I received the same error.
2. I used the Linux Command Line textbook and searched
	for each type of input (ie. gzip) to find which
	command to use for each argument.
3. The most challenging aspect of writing this script
	was figuring out how to overcome the "empty argument"
	error.

Activity 2
1. First, I tried to display random messages to the user
	using a case statement. That did not work, so I instead
	made a function, cowtellingfortune. I used "shuf" to choose
	a random line before EOF, the end of file, to display to
	a user when an argument was provided.
	
2. I handled signals by adding a Trap for my early_exit function.

3. I read input from the user by using read -p

4. The most challenging aspect of writing this script was incorporating
	cowsay.

Activity 3

1) I scanned 'xavier.h4x0r.space' for an HTTP port in the
	9000-9999 range by running
	$nmap -v -PN xavier.h4x0r.space
	
	It discovered 4 open ports total, but only two in the
	9000-9999 range: 911 and 9876
	
2) Next, I tried to access the HTTP server by running
	$ nc xavier.h4x0r.space 9111

3)Some creature asked who I may be and I said Sam. 

4)It asked what message I had for it and I said hi.

5) It said it didn’t understand my message and told me 
	to go back  to the DOORMAN or BOBBIT. 

6) I tried typing DOORMAN and BOBBIT and it said command not 
	found, 
	
7)	so I used INT (Ctrl-C) and accessed the other 
	server by running $ nc Xavier.h4x0r.space 9876
	This yielded the same result, 
	
8)	so I put http://xavier.h4x04.space:9876 into my web browser.

	This yielded the following:
________________________________________ / Halt! Who goes there? \ | | | If you seek the ORACLE, you must come | | back and _request_ the DOORMAN at | | /{NETID}/{PASSCODE}! | | | | To retrieve your PASSCODE you must | | first _find_ your LOCKBOX which is | | located somewhere in | | ~pbui/pub/oracle/lockboxes. | | | | Once the LOCKBOX has been located, you | | must use your hacking skills to | | _bruteforce_ the LOCKBOX program until | | it reveals the passcode! | | | \ Good luck! / ---------------------------------------- \ \ ,__, | | (oo)\| |___ (__)\| | )\_ | |_w | \ | | || * Cower....

9)	I then cd’d to ~pbui/pub/oracle/lockboxes

10)	I ran $ls

11)	There were a lot of numbers, so I then ran $find ./ -name ‘sscaglio.*’

	This resulted in a lot of files with gibberish/sscaglio.(numbers 1 through 100) and gibberish/sscaglio.lockbox

12) I ran the string command on the lockbox 
	$strings e15b654/a13040ea/bdc2410/06068f11/sscaglio.lockbox

	This gave me a lot of lines with possible passwords

13)	I tried to put this into a for loop, but it returned an error. I decided to cd into e15b654/a13040ea/bdc2410/06068f11 so I didn’t need to type as much every single time by running
	$ cd e15b654/a13040ea/bdc2410/06068f11 

14)	Next, I ran $strings sscaglio.lockbox

	This gave me a lot of lines with possible passwords (just like before)

15)	Next I put this in a for loop by running the following
	$ for string in `strings sscaglio.lockbox`; do ./sscaglio.lockbox $string; done

	I got an output 30fa82bd64ccd51c9bb35c10880ab749

16)	I crawled into Bobbit’s DMs and messaged him the following:
	sscaglio [10:56 PM] 
	!verify sscaglio 30fa82bd64ccd51c9bb35c10880ab749

	Bobbit [10:56 PM] 
	Greetings sscaglio! Please tell the ORACLE the following MESSAGE: ZmZwbnR5dmI9MTQ4NjI2Njk5OQ==

11)	Now that I have a message, I decided to attempt to reach the port again by once again running $ nc xavier.h4x0r.space 9111

12)	I LOVE THIS DRAGON AND DONKEY TALKING TO ME #OTP

13)	The dragon (who ironically is the quiet one in Shrek) asked me who I may be.

14)	I said Sam

15) It asked me for my message and I sent ZmZwbnR5dmI9MTQ4NjI2Njk5OQ==

16) It said the message wasn’t meant for me

17) I ran $ nc xavier.h4x0r.space 9111 again and got a less impressive lion talking to me.

18) The lion asked for my name and I said sscaglio

19) It asked me for my message and I sent ZmZwbnR5dmI9MTQ4NjI2Njk5OQ==

20) It asked why I took so long and I answered.

21) It said I completed the journey, so I documented that here.

