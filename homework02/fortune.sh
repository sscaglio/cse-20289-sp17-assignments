#!/bin/sh


early_exit(){
	cowsay Youch, you don't believe me
	exit 1
}

trap early_exit HUP INT TERM

usage(){
	echo "usage: $(basename $0)"
}

cowtellingfortune() {
		shuf << EOF | head -n 1
		It is certain
		It is decidedly so
		Without a doubt
		Yes, definitely
		You may rely on it
		As I see it, yes
		Most likely
		Outlook good
		Yes
		Signs point to ys
		Reply hazy try again
		Ask again later
		Better not tell you now
		Cannot predict now
		Concentrate and ask again
		Don't count on it
		My reply is no
		My sources say no
		Outlook not so good
		Very doubtful
		EOF
}

read -p "Enter a question!" QUESTION

while [ QUESTION == {} ]; do
		read -p "Enter a question!" QUESTION
done

cowsay $(cowtellingfortune)
