#!/bin/sh

#include <iostream>

if [ "$#" -eq 0 ]; then
	echo "No arguments supplied"
	echo "${n}" 1>&2
	exit 1
fi

n="1"

while ["${n}" -lt "$#"]; do
	if [ -f ${n} ]; then
		case ${n} in
			*.tar.gz) tar xvzf ../${n} ;;
			*.tgz) tar xvzf ../${n} ;;
			*.tar.bz2) tar xvjf ../${n} ;;
			*.tbz) tar xvjf ../${n} ;;
			*.tbz2) tar xvjf ../${n} ;;
			*.bz2) bunzip2 ../${n} ;;
			*.tar.xz) tar xvJf ../${n} ;;
			*.txz)tar jxvf ../${n} ;;
			*.zip) unzip ../${n} ;;
			*.jar)jar xf ../${n} ;;
			*) echo "Error: file form not supported" ;;
		esac
		n = n+1
		exit 0
	else
		echo "${n} does not exist."
		n = $[$n+1]
		exit 1
	fi
done
