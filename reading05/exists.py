#!/usr/bin/env python2.6

import sys
import os

for arg in sys.argv[1:]:
	if os.path.isfile(arg) == 1:
		print('%s exists.' % arg),
	else:
		print('%s does not exist.' % arg),
		sys.exit(1)
