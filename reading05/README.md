Reading 05
==========

1.

	1. The purpose of 'import sys' imports- or gives the
		user access to the objects of- the sys module
		which holds system-specific information. It provides
		access to some of the variables maintained by the
		interpreter "and to functions that interact strongly
		with the interpreter" (Python Software Foundation).
	
	2. for arg in sys.argv[1:]:
		This line starts a for loop for every command line
		argument indexed 1 to whatever the last value is
		(not including the first argument which would be
		indexed 0) in the list of command line arguments
		passed to a Python script

	3. In python, commas can be used as valid syntax
		to separate elements of a list.


2. 

	1. The command line arguments are parsed by the loop
		'while len(args) and args[0].startswith('-') and len(args[0]) > 1'
		This is a compound while loop that ensures that the
		length of the array of arguments is greater than one
		then checks that it is true that the first argument 
		(args[0]) starts with a dash then checks that the 
		length of the first argument (args[0]) is greater
		than one. If all of these conditions are met, it
		proceeds with processing the arguments.
	
	2.  The purpose of the first given code block is to
		check if the length of the array of arguments
		is equal to zero then, if that is true, append
		or add to the end of the list a dash ('-').
		The purpose of the second given code block is
		to check if the path is the dash that was
		appended to the arguments array before. If
		it is, the text stream equals the standard
		input and if it is not then the stream
		equals whatever the path is.
		
	3. The line=line.rstrip() returns a copy of the line
		in which all default whitespace characters have
		been stripped from the end of the line.
		This is not necessary because it is good 
		programming practice to put whitespaces after
		commas, arround arithmetic operators, etc
