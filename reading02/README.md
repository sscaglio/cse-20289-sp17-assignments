Reading 02
==========
1. /bin/sh exists.sh
2. chmod 755 exists.sh
3. ./exists.sh
4. The "#!/bin/sh" line is the path to the script interpreter.
5. If you run the script  with the arguments "*" you would
	 call all parameters $1...whatever without preserving
	 and whitespace, and quoting, so you would output:
	 "README.md" "exists!" "exists.sh" "exists!"
6. The $1 that appears in the script is the first parameter
	beyond the basename of the program that exists.
7. The test -e $1 tests if an argument is a file that exists.
8. This script tests if a file exists then outputs either
	"*filename* exists!" if it does exist or
	"*filename* does not exist!" if it doesn't exist


