#!/bin/sh

if [ "$#" -eq "0" ]; then
	echo "Error: no arguments were given"
fi

while [ "$#" -gt "0" ]
do
	if [ -e "$1" ]; then
		if [ $? -eq 0 ]; then
			echo "$1 exists!"
		else
			echo "Error"
		fi
		shift
	else
		if [ $? -eq 0 ]; then
			echo "$1 does not exist!"
		else
			echo "Error"
		fi
		shift
	fi

done
