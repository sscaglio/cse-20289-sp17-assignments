Reading 09
==========

1.
	1. The size of s is 12. You get this by summing the
		memory allocated by char * (8) and the memory
		allocated by int (4) for the grand total of 12.
		The size of u is 8. You get this by finding the
		memory allocated by the largest data type, in
		this case char * (8).
	2. A struct is a basic data structure that can be
		used as a collection of fields- which could be
		different data types. In other words, it can
		store multiple values and variables. On the other
		hand, a union is a single value that can be stored
		as one of any number of formats within the
		same position in memory. As a result, in terms of
		memory allocation, the size of a struct is the sum 
		of all the different data types listed in its 
		definition, while a union is just the value used
		to allocated the single largest data type.

2.
	1. "fate is inexorable!"
	2. According to the Princeton site on data representation
		linked in the reading, in many programs you need
		to remove the signed extended bits. This is because
		the representation of the number isn't changed based
		on the characterization of "signed" v "unsigned."
		Setting values in struct Thing t yields the final
		message because it allows the values to be stored
		as different data types that ultimately come together
		to produce the message.
	3. When we  cast values in c we are designating variables
		as one type or another and thus letting the computer
		know how to interpret it. This tells us that different
		data types require more or less memory based on how
		they are stored- and how many bits are required to
		store them. Everything goes back to bits because
		on the most elementary level, computers work with 1's
		and 0's and C is an assembly level language.

3.
	1. A collision is an the occurence of the same output-
		which can be a hash value, checksum, or digest-
		being produced by two different inputs to a function.
		For our interests, since every element in a hash table
		is associated with a bucket, we don't want multiple
		items to be placed into the same bucket or else we
		will have a collision.
 
	2. In separate chaining, each bucket is independent and has
		some sort of list of entries with the same index.
		Collision can be resolved by separate chaining because
		it lists entries with the same index.
		
	3. In open addressing, all entry records are stored in the
		bucket array itself. When a new entry has to be inserted,
		the buckets are examined until the computer can find an
		open spot. Hash collision can be resolved by open
		addressing with linear probing because it places new
		records at the next free location.
4. 
	1. 
		| Bucket	| Value	|
		|-----------|-------|
		| 0			| 		|
		| 1			|		|
		| 2			| 2, 72	|
		| 3			| 3, 13	|
		| 4			| 14	|
		| 5			|		|
		| 6			| 56	|
		| 7			| 7		|
		| 8			| 78, 68|
		| 9			| 79	|
	
	2. 
		| Bucket	| Value	|
		|-----------|-------|
		| 0			| 14	|
		| 1			|		|
		| 2			| 2		|
		| 3			| 3		|
		| 4			| 72	|
		| 5			| 13	|
		| 6			| 56	|
		| 7			| 7		|
		| 8			| 78	|
		| 9			| 79	|
