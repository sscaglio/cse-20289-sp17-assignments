reading09 - Grading
===================

**Score**: 3.5/4

Deductions
----------
-0.25 - s is 16 bytes
-0.25 - open addressing table is partially wrong

Comments
--------
Good work!
