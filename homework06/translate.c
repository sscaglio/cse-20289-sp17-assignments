/* translate.c: string translator */

#include "stringutils.h"

#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* Globals */

char *PROGRAM_NAME = NULL;
enum{ 
  STRIP = 1<<1,
  REVERSE = 1<<2,
  REVERSE_WORDS = 1<<3,
  LOWERCASE = 1<<4,
  UPPERCASE = 1<<5
};

/* Functions */

void usage(int status) {
  fprintf(stderr, "Usage: %s SOURCE TARGET\n\n", PROGRAM_NAME);
  fprintf(stderr, "Post Translation filters:\n\n");
  fprintf(stderr, "   -s     Strip whitespace\n");
  fprintf(stderr, "   -r     Reverse line\n");
  fprintf(stderr, "   -w     Reverse words in line\n");
  fprintf(stderr, "   -l     Convert to lowercase\n");
  fprintf(stderr, "   -u     Convert to uppercase\n");
  exit(status);
}

void translate_stream(FILE *stream, char *source, char *target, int mode){
  /* TODO */
  char buffer[BUFSIZ];
  while(fgets(buffer, BUFSIZ, stream)){
    string_chomp(buffer);
    string_translate(buffer, source, target);
    }
    if(mode & STRIP){
      string_strip(buffer);
    }
    if(mode & REVERSE){
      string_reverse(buffer);
    }
    if(mode & REVERSE_WORDS){
      string_reverse_words(buffer);
    }
    if(mode & LOWERCASE){
      string_lowercase(buffer);
    }
    if(mode & UPPERCASE){
      string_uppercase(buffer);
    }
    printf("%s\n", buffer);
}

/* Main Execution */
int main(int argc, char *argv[]){
  /* Parse command line arguments */
  int argind = 1;
  int mode = 0;
  char* source = "";
  char* target = "";
  PROGRAM_NAME = argv[0];
  
  while ((argind < argc) && (strlen(argv[argind]) > 1) && (argv[argind][0] == '-')){
    char *arg = argv[argind++];
    switch (arg[1]){
      case 'h':
        usage(0);
        break;
      case 's':
        mode |= STRIP;
        break;
      case 'r':
        mode |= REVERSE;
        break;
      case 'w':
        mode |= REVERSE_WORDS;
        break;
      case 'l':
        mode |= LOWERCASE;
        break;
      case 'u':
        mode |= UPPERCASE;
        break;
      default:
        usage(1);
        break;
    }
  }

  if (argind + 2 <= argc){
    source = argv[argind + 1];
  }
  if (argind + 1 <= argc){
    target = argv[argind + 1];
  }
  /* Translate Stream */
  translate_stream(stdin, source, target, mode);

  return EXIT_SUCCESS;
}

/* vim: set sts=4 sw=4 ts=8 expandtab ft=c: */
