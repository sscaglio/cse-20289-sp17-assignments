Homework 06
===========

Activity 1

1. string_reverse_words: This function works by using isspace to isolate words with whitespace
	between them. Once it encounters a space, it calls the string_reverse_range function
	up to the letter before the space in order to only reverse the word. Its time complexity
	is O(n^2) as it goes through a nested while loop, but its space complexity is only O(1)
	since the amount of space it takes up is unaffected by size or the arguments.
2. string_translate: This function works by using a nested for loop to check if the pointer is
	pointing to the character we want to change. If it is, It changes what we want to change it
	from to what we want to change it to. It, as aforementioned, uses a nested for loop, so
	its time complexity is O(n^2); however, it takes up constant space, so its space
	complexity is only O(1).
3. string_to_integer: This function works by starting at the first non-null character, c. From
	there, it iterates backwards through the string until encountering the end of the string.
	The function first checks if the character is a letter then subtracts '0', or '0' and 7 if 
	it is an alphabetical character. The value is raised to its respective power then added to 
	the total. The time complexity is O(n) because it only utilizes one loop, and space is
	still constant so the space complexity is again O(1).
4. The biggest difference between a shared library and a static library is that a shared 
	library is referenced by programs at run-time; the program itself only makes reference
	to the code that it uses in the shared library. On the other hand, static libraries
	are directly linked into the program when it is compiled- it takes copies of the code
	that it uses from the library and virtually copy and pastes it into the program.
	The static library, .so, is larger. Shared libraries are meant to reduce the amount
	of code that is duplicated in each program to make use of the library, so it makes sense
	that static libraries would take up more room.

Activity 2: Translate

1. I parsed the command line arguments using the following while loop:
		 while ((argind < argc) && (strlen(argv[argind]) > 1) && (argv[argind][0] == '-'))
	Basically, I made sure that the argument index was less than the total number
	of arguments, the length of the argument was greater than 1, and the argument began
	with the '-' character to indicate a flag, before proceeding. I then used a case
	statement for arg[1] (not arg[0] since that would be the '-' character). I then used
	translate_stream to match up the indicated mode with one of the modes that is defined
	and use functions from the library accordingly.

2. The difference between a static executable and a dynamic executable is that the static
	executable has the library built into it. As such, translate-static is much larger.
	The translate-static executable works because it already has the library built into it;
	however, the translate-dynamic executable would not work without a path for the library
	specified.