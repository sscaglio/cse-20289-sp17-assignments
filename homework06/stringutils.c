/* stringutils.c: String Utilities */

#include "stringutils.h"

#include <ctype.h>
#include <string.h>
#include <math.h>

/**
 * Convert all characters in string to lowercase.
 * @param   s       String to convert
 * @return          Pointer to beginning of modified string
 **/
char * string_lowercase(char *s){
  char *c = s;
  for (char * c = s; *c!='\0'; c++){ //increments until it hits the end of the string
    *c = tolower(*c);
  }
  return s;
}

/**
 * Convert all characters in string to uppercase.
 * @param   s       String to convert
 * @return          Pointer to beginning of modified string
 **/
char * string_uppercase(char *s){
  char *c = s;
  for (char * c = s; *c!='\0'; c++){ //increments until it hits the end of the string
    *c = toupper(*c);
  }
  return s;
}

/**
 * Returns whether or not the 's' string starts with given 't' string.
 * @param   s       String to search within
 * @param   t       String to check for
 * @return          Whether or not 's' starts with 't'
 **/
bool string_startswith(char *s, char *t){
  int tLength = strlen(t);
  int sLength = strlen(s);
  if(sLength < tLength){
    return false;
  }
  else{
    for (char* x = t; *x != '\0'; x++){
      if (*x != *s){
        return false;
      }
      s++;
    }
  }
  return true;
}

/**
 * Returns whether or not the 's' string ends with given 't' string.
 * @param   s       String to search within
 * @param   t       String to check for
 * @return          Whether or not 's' ends with 't'
 **/
bool string_endswith(char *s, char *t){
  int tLength = strlen(t);
  int sLength = strlen(s);
  if(sLength < tLength){
    return false;
  }
  else{
    for (char* x = t; *x != '\0'; x++){
      if (*x != *s){
        return false;
      }
      s++;
    }
  }
  return true;
}

/**
 * Removes trailing newline (if present).
 * @param   s       String to modify
 * @return          Pointer to beginning of modified string
 **/
char *	string_chomp(char *s){
  int sLength = strlen(s);
  char *c = s + sLength - 1;
  
  if(*c == '\n'){
    *c = '\0';
  }
  return s;
}

/**
 * Removes whitespace from front and back of string (if present).
 * @param   s       String to modify
 * @return          Pointer to beginning of modified string
 **/
char * string_strip(char *s) {
    int sLength = strlen(s);
  while(isspace(*s)){
    *s = '\0';
    s++;
  }
  while(isspace(*(s + sLength - 1))) {
    *(s + sLength - 1) = '\0';
    slen--;
  }
  return s; 
}

/**
 * Reverses a string given the provided from and to pointers.
 * @param   from    Beginning of string
 * @param   to      End of string
 * @return          Pointer to beginning of modified string
 **/
static char * string_reverse_range(char *from, char *to){
  char* c = from;  
  char temp;
  while(to > c){
    temp = *to;
    *to = *c;
    *c = temp;
    to--;
    c++;
  }
  return from;
}

/**
 * Reverses a string.
 * @param   s       String to reverse
 * @return          Pointer to beginning of modified string
 **/
char * string_reverse(char *s){
  string_reverse_range(s, s + strlen(s) - 1);
  return s;
}

/**
 * Reverses all words in a string.
 * @param   s       String with words to reverse
 * @return          Pointer to beginning of modified string
 **/
char * string_reverse_words(char *s){
    s = string_reverse(s);
    char *d;
    char *c = s;
      while (*c) {
        d = c;
        while (!isspace(c)) {
          c++;
        }
        d = string_reverse_range(d, c-1);
        c++;
      } 
    return s;
}

/**
 * Replaces all instances of 'from' in 's' with corresponding values in 'to'.
 * @param   s       String to translate
 * @param   from    String with letter to replace
 * @param   to      String with corresponding replacment values
 * @return          Pointer to beginning of modified string
 **/
char * string_translate(char *s, char *from, char *to){
  for(char *c = s; *c != '\0' ; c++) {
    *to_c
    for(char *d = from; d != '\0'; d++) {
      if(*c == *d) {
        *c=*to_c;
      }
      to_c++;
    }
  }
  return s;
}

/**
 * Converts given string into an integer.
 * @param   s       String to convert
 * @param   base    Integer base
 * @return          Converted integer value
 **/
int	string_to_integer(char *s, int base){
  string_uppercase(s);
  char *c = s + strlen(s) - 1;
  int i;
  int power = 0;
  int endNum = 0;
  while(*c != '\0'){
    if(isalpha(*c)){
      i = *c - '0' - 7;
      endNum = endNum + i*pow(base, power);
    }
    else if(isalnum(*c)){
      i = *c - '0';
      endNum = endNum + i*pow(base, power);
    }
    power++;
    c--;
  }
  return endNum;
}
/* vim: set sts=4 sw=4 ts=8 expandtab ft=c: */
