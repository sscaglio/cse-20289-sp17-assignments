#!/usr/bin/env python2.7

import sys

linenum = 0

for line in sys.stdin:
	linenum = linenum + 1
	for word in line.strip().split():
		word = word.lower()
		word = [l for l in word if l.isalpha()]
		word = ''.join(word)
		print '{}\t{}'.format(word, linenum)
