#!/usr/bin/env python2.7

import functools
import hashlib
import itertools
import multiprocessing
import os
import string
import sys
import getopt

# Constants

ALPHABET    = string.ascii_lowercase + string.digits
ARGUMENTS   = sys.argv[1:]
CORES       = 1
HASHES      = 'hashes.txt'
LENGTH      = 1
PREFIX      = ''

# Functions

def usage(exit_code=0):
	print '''Usage: {} [-a alphabet -c CORES -l LENGTH -p PATH -s HASHES]
	-a ALPHABET Alphabet to use in permutations
	-c CORES    CPU Cores to use
	-l LENGTH   Length of permutations
	-p PREFIX   Prefix for all permutations
	-s HASHES   Path of hashes file'''.format(os.path.basename(sys.argv[0]))
	sys.exit(exit_code)

def md5sum(s):
	''' Generate MD5 digest for given string.

	>>> md5sum('abc')
	'900150983cd24fb0d6963f7d28e17f72'

	>>> md5sum('wake me up inside')
	'223a947ce000ce88e263a4357cca2b4b'
	'''
	# TODO: Implement
	m = hashlib.md5()
	m.update(s)
	return m.digest()

def permutations(length, alphabet=ALPHABET):
	''' Yield all permutations of alphabet of specified length.

	>>> list(permutations(1, 'ab'))
	['a', 'b']

	>>> list(permutations(2, 'ab'))
	['aa', 'ab', 'ba', 'bb']

	>>> list(permutations(1))       # doctest: +ELLIPSIS
	['a', 'b', ..., '9']

	>>> list(permutations(2))       # doctest: +ELLIPSIS
	['aa', 'ab', ..., '99']
	'''
	# TODO: Implement
		
	permAlpha = itertools.product(ALPHABET, LENGTH)
	print permAlpha
	return permAlpha
	
def smash(hashes, length, alphabet=ALPHABET, prefix=''):
	''' Return all password permutations of specified length that are in hashes

	>>> smash([md5sum('ab')], 2)
	['ab']

	>>> smash([md5sum('abc')], 2, prefix='a')
	['abc']

	>>> smash(map(md5sum, 'abc'), 1, 'abc')
	['a', 'b', 'c']
	'''
	# TODO: Implement
	hashdict = {}
	for i in permutations(length, alphabet):
		print i
		hashdict[md5sum(i)] = i
	l = [ hashdict[hash] for hash in hashes  if hash in hashdict ]
	return l
	pass

# Main Execution

if __name__ == '__main__':

	# TODO: Parse command line arguments
	try:
		opts, args = getopt.getopt(sys.argv[1:], "ha:c:l:p:s:", [])
	except getopt.GetoptError as e:
		print >>sys.stderr, e
		sys.exit(1)
		
	for opt, arg in opts:
		if opt == '-h':
			usage(0)
		elif opt == '-a':
			ALPHABET = arg
		elif opt == '-c':
			CORES = int(arg)
		elif opt == '-l':
			LENGTH = int(arg)
		elif opt == '-p':
			PREFIX = arg
		elif opt == '-s':
			HASHES = arg
		else:
			usage(1)

	# TODO: Load hashes set
	hashes = []
	with open(HASHES) as f:
		for line in f:
			hashes.append(line.rstrip())
	
	# TODO: Execute smash function to get passwords

	passlist = smash(HASHES, LENGTH, ALPHABET, PREFIX)

	# TODO: Print passwords
	
	with open('passlist.txt' 'w+') as out:
		for p in passlist:
			out.write(p + '\n')
			print p
	
# vim: set sts=4 sw=4 ts=8 expandtab ft=python:
