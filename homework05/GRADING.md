Homework 05 - Grading
=====================

**Score**: 7.5/15

Deductions
----------
-.25 hulk.py: md5sum should use hexdigest instead of digest
-.25 hulk.py: does not use prefix properly
-.25 hulk.py: loaded hashes should not be in a set or dict instead of a list
-.5 hulk.py: does not use multiprocessing properly
-.5 test_hulk.py: some tests fail
-1 doctests: most tests fail
-1 deadpool: <2000 password
-2 README.md Act1: No answers
-.25 iv_map.py: handles some punctuation but should not remove -
-.5 test_iv.py: some tests fail
-1 README.md Act2: No answers

Comments
--------
