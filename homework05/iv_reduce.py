#!/usr/bin/env python2.7

import sys
import collections

counts = collections.defaultdict(set)
for line in sys.stdin:
	word, linenum = line.split('\t', 1)
	counts[word].add(int(linenum))

for word,linenum in sorted(counts.items()):
	print'{}\t{}'.format(word,' '.join(map(str, sorted(linenum))))
