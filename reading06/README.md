Reading 06
==========
1. MapReduce is a programming model and an
associated implementation for processing and
generating large data sets. It is useful for
problems including but not limited to:
	- large-scale machine learning problems
	- clustering problems for the Google News
		and Froogle products
	- extraction of data used to produce
		reports of popular queries
	- extraction of properties of web pages
		for new experiments and products
	- large-scale graph computations

2. The three phases of a typical MapReduce
workflow are
	1) "Map" step: in this step the map()
		function is applied by each worker
		node to the local data and the output
		is written to temporary storage
	2) "Shuffle" step: in this step, data is
		redistributed by worker notes based
		on the output keys so that all data
		belonging to one key will be located
		on the same worker node
	3) "Reduce" step: Each group of output
		data is processed by the worker
		nodes, per key, in parallel.
