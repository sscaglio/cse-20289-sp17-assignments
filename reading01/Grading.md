3.4/4

-0.2 2D: The square brackets produce an incorrect command. If you removed them, the command would work.

-0.2 2E: Same square brackets problem

-0.2 4C: Use ctrl + d. I think you may misunderstand the questions. The question is saying that a process is reading from standard in, and you want to tell it that you're done entering information into the terminal, so for that, you use ctrl + d. bg would do nothing for this problem.
