# Samantha Scaglione, sscaglio
## CSE 20289-01
### 22 January 2017

#### **_Reading 1_**

1. Given the following command:
		$ du -h /etc/ 2> /dev/null | sort -h > output.txt

	a.  The purpose of the "|" or the pipe operator is
		to pipe the standard output of one command into
		the standard input of another.

	b.  The purpose of "2> /dev/null" is to redirect the
		standard error (of which the file descriptor is
		2) to the bit bucket (which accepts input and
		does nothing with it)
	
	c.  The purpose of the "> output.txt" is to redirect
		the output to a text file called "output.txt"
	
	d.  The purpose of the "-h" flags is to put the
		output in "human readable" form by displaying
		file sizes in human readable format rather than
		in bytes.
	
	e.	The command is not the same as the one above because
		it would take the standard error of the sort command
		instead of the standard error of the du command.


2. Given the input of ls,

	a.	You could concatenate all the files from 2002 into
		one file by running cat[2002*]
		
	b.	You could concatenate all the files from the month
		of December into one file by running cat[*12]
		
	c.  You could concatenate all the files from the month
		of January to June into one file by running
		cat[*{1-6}]
		
	d. 	You could view all the files with an even year and 
		an odd month by running 
		cat[200{2,4,6}-{01,03,05,07,09,11}]|more
		
	e.  You could view the contents of all the files from
		2002 to 2004 in the months of September to December
		by running
		cat[200{2,3,4}-{09,10,11,12}]|more
		
3. Given the input of ls -l

	a. 	The last two files- the files with the fourth
		character "x"- are executable by the owner.
		(the Huxley and Tux files)
	
	b.	The last two files- the files with the fifth
		character "r"- are readable by members of
		the users group. (the Huxley and Tux files)
		
	c. 	None of the files are writable by anyone
		because none have a ninth character "w"
	
	d. 	In order to give Tux the same permissions as
		Huxley, you would run "chmod 755 Tux"
		
	e.	In order to give Tux the same permissions as
		Beastie, you would run "chmod 600 Tux"
		
4. If I ran the bc command,

	a.  In order to suspend (or temporarily stop) the
		bc process, you would run "kill -19 PID"
		If it's in the foreground, you could also press
		Ctrl-z.
		
	b. 	In order to continue a suspended process,
		you would run "kill -18 PID"
		
	c.	You could indicate the end of input to the bc
		process by running "bg %1" because a process
		in the background is immune from keyboard input.
		
	d.  If the bc process was still running, you would
		terminate it by running "kill -9 PID" because
		the KILL signal should be used as a last resort
		when other termination signals fail.
	
	e. 	If the process was still still running, I would
		terminate it from another terminal by running
		"kill -15 PID" because if the program is still
		*alive* enough to receive signals, it will
		terminate.
		
		
