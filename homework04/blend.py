#!/usr/bin/env python2.7

import atexit
import os
import re
import shutil
import sys
import tempfile

import requests

# Global variables

DELAY       = 20
STEPSIZE    = 5
REVERSE     = False

# Functions

def usage(status=0):
    print '''Usage: {} [ -r -d DELAY -s STEPSIZE ] netid1 netid2 target
    -r          Blend forward and backward
    -d DELAY    GIF delay between frames (default: {})
    -s STEPSIZE Blending percentage increment (default: {})'''.format(
        os.path.basename(sys.argv[0]), DELAY, STEPSIZE
    )
    sys.exit(status)

# Parse command line options

args = sys.argv[1:]

while len(args) and args[0].startswith('-') and len(args[0]) > 1:
    # TODO: Parse command line arguments
    arg = args.pop(0)
    if arg == '-h':
        usage(0)
    elif arg == '-d':
        DELAY = int(args.pop(0))
    elif arg == '-r':
        REVERSE = True
    elif arg == '-s':
        STEPSIZE = int(args.pop(0))
    else:
        usage(1)

if len(args) != 3:
    usage(1)

netid1 = args[0]
netid2 = args[1]
target = args[2]

# Main execution

# TODO: Create workspace
workspace = tempfile.mkdtemp()

# TODO: Register cleanup
def cleanup():
    shutil.rmtree(workspace)
atexit.register(cleanup)

# TODO: Extract portrait URLs
def search_portrait(netid):
    r = requests.get('http://engineering.nd.edu/profiles/{}'.format(netid))
    url = re.findall('http://engineering.nd.edu/profiles/{}/@@images/.*jpeg'.format(netid), r.content)
    if r.status_code != 200:
    	print 'Not found'
        sys.exit(1)
    return url

firstportrait = search_portrait(netid1)
secondportrait = search_portrait(netid2)

# TODO: Download portraits
def downloadPortraits(netid, url):
    p = requests.get(url)
    with open(workspace + '/'+netid+'.jpeg', 'wb') as fs:
        fs.write(p.content)

downloadPortraits(netid1, firstportrait[0])
downloadPortraits(netid2, secondportrait[0])

    # TODO: Generate blended composite images
composite = []
for num in range(0, 101, STEPSIZE):
    filename = workspace + '/' + str(num)+ '.jpeg' 
    command = 'composite -blend {} {}.jpeg {}.jpeg {}'.format(num, workspace+'/'+netid1, workspace+'/'+netid2, filename)
    os.system(command)
    print "Generating {}".format(filename)
    composite.append(filename)

# TODO: Generate final animation
if REVERSE == True:
    for num in range(100, -1, -(STEPSIZE)):
        filename = workspace + '/' + str(num) + '.jpeg'
        command = 'composite -blend {} {}.jpeg {}.jpeg {}'.format(num, workspace+'/'+netid1, workspace+'/'+netid2, filename)
        os.system(command)
        print "Generating {}".format(filename)
        composite.append(filename)

    os.system('convert -delay {} {} {}'.format(DELAY, ' '.join(composite), target))
else:
    os.system('convert -delay {} {} {}'.format(DELAY, ' '.join(composite), target))


