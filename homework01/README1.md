#Homework 01
#===========
##*Samantha Scaglione*
##*Professor Bui*
##*CSE 20289*
##*27 January 2016*

###Activity 1
1. Using the _fs listacl_ command to view the ACLs on my HOME directory,
	my Private directory, and my Public directory,
	
	a. The differences in the ACLs for these three folders is the
		presence or lackthereof of "nd_campus" and "system:authuser."
		While HOME has nd_campus 1 and system:authuser 1, Public has
		nd_campus rlk and system:authuser rlk. Private has neither.
		
	b. Anyone has access to the Public (nd_campus can rlk- read, lookup,
		and lock), but the Private give nd_campus no such accesses.
		
2. Using the touch command to create a file at _/afs/nd.edu/web/sscaglio.txt_ 

	a. The command resulted in a "Permission Denied" error. I was not
		able to create the specified file.
		
	b. Considering this result, the ACLs take precedence on AFS-
		the Unix user permission bits are the final modifiers
		of access to the file.
		
###Activity 2 
| Command							| Elapsed Time |
|-----------------------------------|--------------|
| cp * ~/images						| 0.041 s	   |
| mv images pixmaps					| 0.001 s	   |
| mv pixmaps /tmp/sscaglio-pixmaps  | 0.018 s	   |
| rm -r /tmp/sscaglio-pixmaps		| 0.003 s	   |

1. When renaming the folder, you are only changing one attribute
	 of	file; however, when moving the folder, you are creating
	 a new folder, copying all of the old folder's contents and
	 attributes to the new folder, then deleting the original folder.
	 Part of this second process is, in fact, naming the new folder.
	 Therefore, since the former is one component of the processes
	 required for the latter, it follows that the latter process 
	 would take longer.
	 
2. Similarly, removing the original file is part of moving a file
	and is therefore faster than the move operation. As aforementioned,
	in order to move a file, the computer must create a new folder,
	copy all of the old folder's contents and attributes to the new
	folder, then delete the original folder. Since deleting the folder
	is just one component of the process required for the move operation,
	it follows that the move operation would take longer than the
	removal operation.
	
###Activity 3
1. bc < math.txt
2. bc < math.txt > results.txt
3. bc < math.txt > results.txt 2> /dev/null
4. cat math.txt | bc
	This is considered less efficient than I/O because it adds an extra
	process.
	
###Activity 4
1. ls /etc/passwd/sbin/nologin | wc -l
2. users | wc -l
3. du -a /etc | sort - S -r | head -n 5 2> /dev/null
4. ps aux | grep bash | wc -l

###Activity 5
1. 
	a. 	I tried to ctrl-c, and it taunted me.
		I also tried to ps before interrupting
		it and nothing happened.
	b.	I used ctrl-z to interrupt it then
		ps to find its PID then kill -9 7251
		(where 7251 is its PID) in order to
		kill it. Lastly, I ran ps again to make
		sure that I killed it.

2. 
	a.	I used
		ps -e | grep TROLL | awk '{print $1}' | kill -9 $(xargs)
	b.  I used
		 killall -a 9 TROLL

3. I tried using ctrl-k, ctrl-u, alt-d, and alt-backspace without
	producing any results.
