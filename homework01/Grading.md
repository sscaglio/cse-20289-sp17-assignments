13.25/15

-0.5 Activity 4 - #1: You should use grep to look for /sbin/nologin in /etc/passwd and pipe the result to wc -l

-0.25 Activity 4 - #2: Some users are listed multiple times in users, so you must pipe the output through uniq before counting

-0.5 Activity 4 - #3: You should use 2> /dev/null after du rather than after head, and your path should be /etc/* rather than /etc becuase you will get /etc in du with your current path, which you dont want

-0.5 Activity 5 - #3: You should have found up to 5 interesting outputs by sending signals 1, 7, 10, 12, and 14 to TROLL
