#!/bin/sh

# ethnic.sh

URL=https://www3.nd.edu/~pbui/teaching/cse.20289.sp17/static/csv/demographics.csv

count_ethnic() {
    case $1 in
    	"2013") column=2;;
    	"2014") column=4;;
    	"2015") column=6;;
    	"2016") column=8;;
    	"2017") column=10;;
    	"2018") column=12;;
    	"2019") column=14;;
    esac
    	# TODO: Determine appropriate column from first argument
    ethnicity=$2
    curl -s $URL | cut -d ',' -f $column | grep -c $ethnicity
    # TODO:	Ethnic group is second argument
    # TODO extract ethnic data for specified year and group
}

for year in $(seq 2013 2019); do
    echo $year $(count_ethnic $year C) \
    	       $(count_ethnic $year O) \
    	       $(count_ethnic $year S) \
    	       $(count_ethnic $year B) \
    	       $(count_ethnic $year N) \
    	       $(count_ethnic $year T) \
    	       $(count_ethnic $year U)
done
