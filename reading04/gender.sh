#!/bin/sh

# gender.sh

URL=https://www3.nd.edu/~pbui/teaching/cse.20289.sp17/static/csv/demographics.csv

count_gender() {
    case "$1" in
    	"2013") column=1;;
    	"2014") column=3;;
    	"2015") column=5;;
    	"2016") column=7;;
    	"2017") column=9;;
    	"2018") column=11;;
    	"2019") column=13;;
    esac
    	# TODO: Determine appropriate column from first argument
   	gender=$2
    curl -s $URL | cut -d ',' -f $column | grep -c $gender
       # TODO:	Gender is second argument
    # TODO extract gender data for specified year and group
}

for year in $(seq 2013 2019); do
    echo $year $(count_gender $year F) $(count_gender $year M)
done
