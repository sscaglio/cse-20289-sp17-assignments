/* echo_client.c: simple TCP echo client */

#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <netdb.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <unistd.h>

const char *HOST = "localhost";

FILE *socket_dial(const char *host, const char *port) {
    /* Lookup server address information */
    struct addrinfo *results;
    struct addrinfo  hints = {
    	int .ai_family		= AF_UNSPEC,
    	int .ai_socktype	= SOCK_STREAM,
    	int .ai_protocol = 0,
    	struct sockaddr *.ai_addr = NULL,
    	socklen_t .ai_addrlen = NULL,
    };
    int status;
    if ((status = getaddrinfo(host, port, &hints, &results)) != 0) {
    	fprintf(stderr, "getaddrinfo failed: %s\n", gai_strerror(status));
   	}
	return NULL;
    }

    /* For each server entry, allocate socket and try to connect */
    int client_fd = -1;
    for (struct addrinfo *p = results; p != NULL && client_fd < 0; p = p->hints.ai_next) {
	/* Allocate socket */
	if ((client_fd = socket(p->hints.ai_family, p->hints.ai_socktype, p->hints.ai_protocol)) < 0) {
	    fprintf(stderr, "Unable to make socket: %s\n", strerror(errno));
	    continue;
	}

	/* Connect to host */
	if (connect(client_fd, p->hints.ai_addr, p->hints.ai_addrlen) < 0) {
	    fprintf(stderr, "Unable to connect to %s:%s: %s\n", host, port, strerror(errno));
	    close(client_fd);
	    client_fd = -1;
	    continue;
	}
    }

    /* Release allocate address information */
    freeaddrinfo(results);

    if (client_fd < 0) {
    	return NULL;
    }

    /* Open file stream from socket file descriptor */
    FILE *client_file = fdopen(client_fd, "w+");
    if (client_file == NULL) {
        fprintf(stderr, "Unable to fdopen: %s\n", strerror(errno));
        close(client_fd);
        return NULL;
    }

    return client_file;
}

char *PortScanner(const char *host){
	for (int port = 9700; port < 9800; port++){ //for the ports that are within the range
		char *buffer[BUFSIZ]; //create a buffer to store the port numbers
		sprintf(buffer, "%d", port); //put the port numbers into the buffers
		FILE *s = socket_dial(host, buffer); //try socket dial
		if (s != NULL) { //check if it's null; if it's not we found the dropbox!!
			return port; //return the port so we can use it
			printf("%d is open\n", port); //just printing so I can see what port it is
			fclose(s); //close so we don't get an error
		}
}

int main(int argc, char *argv[]) {
    /* Connect to remote machine */
    const char *PORT = PortScanner(HOST);
    FILE *client_file = socket_dial(HOST, PORT);
    if (client_file == NULL) {
        return EXIT_FAILURE;
    }

    /* Read from stdin and send to server */
    char buffer[BUFSIZ];
    while (fgets(buffer, BUFSIZ, stdin)) {
        fputs(buffer, client_file);
        fgets(buffer, BUFSIZ, client_file);
        fputs(buffer, stdout);
    }

    fclose(client_file);
    return EXIT_SUCCESS;
}

/* vim: set expandtab sts=4 sw=4 ts=8 ft=c: */

