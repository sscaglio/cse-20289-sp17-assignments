Reading 03
==========
1. $echo "All your base are belong to us" | tr a-z A-Z

2. $echo "monkeys love bananas" | sed -e 's/monkeys/gorillaz/'

3. $echo "         monkeys love bananas" | sed -e 's/^[ \t]*//'

4. $cat /etc/passwd | grep root | grep /bin/bash | cut -d ':' -f 7

5. $cat /etc/passwd | sed -e 's/\/bin\/bash/\/usr\/bin\/python/g' |
	sed -e 's/\/bin\/csh/\/usr\/bin\/python/g' | sed -e
	's/\/bin\/tcsh/\/usr\/bin\/python/g' | grep python

6. $cat /etc/paasswd | grep -E '4.7:*|*:4.7'

7. $comm -12 file1.txt file2.txt

8. $comm -3 file1.txt file2.txt
