Reading 03 - Grading
===================================
**Score**: 3.5/4

**Grader**: Mimi Chen

Deductions
--------------------
4) (-0.25) Hardcoded the shell that the root uses with <code>grep /bin/bash</code>. The root might not always use /bin/bash, so your command would not match with the shell that the root uses

    cat /etc/passwd | grep ^root | cut -d ':' -f 7

5) (-0) Using three sed commands is very inefficient. You can use one single sed command

    cat /etc/passwd | sed -r 's|/bin/[batc]+sh|/usr/bin/python|'| grep python

6) (-0.25) You command gets anything that has a 4 followed by a single character of any type followed by 7. What you want to do is have **any** number of **digits** in between 4 and 7

    cat /etc/passwd | grep '4[0-9]*7'




Comments
--------------------
