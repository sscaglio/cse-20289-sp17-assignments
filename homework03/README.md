Homework 03
===========
Activity 1

1. I used if, elif, and else statements to
	parse the command line arguments by
	first checking if the number of arguments
	is equal to zero (and if it is set
	the key equal to the default value of
	13), then, if it is not, checking if the
	argument is equal to -h (and if it is
	running the usage function), then, lastly
	if it is neither of the previous cases,
	storing the argument as the key.

2. I constructed the source set (or rather
	the two source sets) by typing the entire
	alphabet in lowercase then again typing
	the entire alphabet in uppercase and
	saving each as variable.
	
3. I constructed the target set by reading in
	the key%26 (to account for a key larger
	than the alphabet and cutting the source 
	set from the first character to the "key" 
	character (or the 13th character if no value
	was inputted). I then cut the source set
	again from the letter after the key value
	(a variable I set equal to key + 1) to the
	26th letter.


Activity 2
1. I parsed the command line arguments by 
	using getopts and a case structure
	in order to implement flags.
	I used getopts "hWd:" because it is
	only checking to see if the -h or
	-W flags are present, but it is
	looking for an input following the
	-d flag (hence the colon).
	
	
2. I removed the comments using the
	sed command and the value of
	DELIM.
	
3. I removed the empty lines using
	tr to delete them.

4. Command line options affected my
	operations because I needed to
	institute an if elif else structure
	in order to allow for the different
	processes dependent on input.
	
Activity 3
1. I parsed command line arguments using
	an if elif else case structure and
	shifting through the arguments.

2. I extracted the zip codes using grep.

3. I filtered by state by instituting the
	value of STATE into my curl to access
	the website. I filtered by city by
	instituting a function that utilized grep.

4. I handled the csv format option by using
	tr to replace the new lines (\n) with
	a comma and a space (, ) then going
	through the same procedure for
	txt. I piped through the functions I
	created.
