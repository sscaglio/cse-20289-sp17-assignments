GRADING - Exam 02
=================

- Identification:   2.25
- Web Scraping:     2.5
- Generators:       5
- Concurrency:      5.5
- Translation:      4.5
- Total:	    19.75

Comments
--------

- 0.5   Translation Q1 - Doesn't run (she-bang, lower())
- 1.0   Translation Q2 - Doesn't run
