#!/usr/bin/env python 2.7

import re
import string

with open('/etc/passwd', 'r') as f:
	text = f.readlines()
	
fields = [line.split(':')[4] for line in text]

regex = '[uU]ser'
result = set([x for x in fields if re.search(regex,x)])
result = result.lower()

for x in sorted(result):
	print x
