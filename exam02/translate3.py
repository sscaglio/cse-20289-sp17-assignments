#!/usr/bin/env python2.7

import os
import sys

x = os.popen('ps aux', 'r')

lines = [str(line.split()[0]) for line in x.read().rstrip().split('\n')]
lines = sorted(lines)
s = set(lines)
print len(s)
